import { Component } from "react";
import image from "../Asset/image/cat.jpg"

class Animals extends Component {
    render () {
        let props = this.props
        return (
            <>
            {
                props.kind === "cat" ? <img src={image} alt="cat image"></img> : <p>meow not found :)</p>
            }
            </>
        )
    }
}

export default Animals;