import logo from './logo.svg';
import './App.css';
import Animals from './components/Animals';

function App() {
  return (
    <div className="App">
      <Animals kind="cat"></Animals>
    </div>
  );
}

export default App;
